import datetime
import logging

from pyrecipe.ports.dbport import DBport
import pony.orm as pny

database = pny.Database("sqlite",
                        "recipes.sqlite",
                        create_db=True)
logger = logging.getLogger()


class StepModel(database.Entity):
    description = pny.Required(pny.LongStr)
    image = pny.Optional(bytes)
    video = pny.Optional(bytes)


class IngredientModel(database.Entity):
    name = pny.Required(str)
    quantity = pny.Required(str)


class RecipeModel(database.Entity):
    name = pny.Required(str, unique=True)
    servings = pny.Optional(int)
    cook_time = pny.Optional(datetime.timedelta)
    prep_time = pny.Optional(datetime.timedelta)
    ingredients = pny.Required(IngredientModel)
    steps = pny.Required(StepModel)


class SqliteDbAdapter(DBport):
    @pny.db_session
    def add_recipe(self, recipe):
        try:
            recipe_model = RecipeModel(**recipe.__dict__)
        except Exception as e:
            logger.error(f'Error adding new recipe: {e}')

    @pny.db_session
    def get_recipe(self, name):
        try:
            recipe_model = RecipeModel[name]
            return recipe_model.__dict__
        except pny.ObjectNotFound as e:
            logger.warning(f'Object not found')
            return None
        except Exception as e:
            logger.error(f'Error retrieving recipe: {e}')

    @pny.db_session
    def get_recipes(self):
        recipes_model = pny.select(r for r in RecipeModel)
        return [recipe.__dict__ for recipe in recipes_model]

    @pny.db_session
    def get_filtered_recipes(self, filters):
        pass


pny.sql_debug(True)
database.generate_mapping(create_tables=True)
