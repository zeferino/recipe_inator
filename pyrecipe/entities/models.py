class Step:
    def __init__(self, description, image=None, video=None):
        self.description = description
        self.image = image
        self.video = video




class Ingredient:
    def __init__(self, name, quantity):
        self.name = name
        self.quantity = quantity


class Recipe:
    def __init__(self, ingredients=[], steps=[], prep_time=None, cook_time=None, servings=None):
        self.servings = servings
        self.cook_time = cook_time
        self.prep_time = prep_time
        self.ingredients: list = ingredients
        self.steps: list = steps

    def add_ingredient(self, ingredient):
        self.ingredients.append(ingredient)

    def rm_ingredient(self, ingredient):
        index = self.ingredients.index(ingredient)
        del self.ingredients[index]

    def add_step(self, step):
        self.steps.append(step)

    def rm_step(self, step):
        index = self.steps.index(step)
        del self.steps[index]

