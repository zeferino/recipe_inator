from abc import ABC


class DBport(ABC):
    def add_recipe(self, recipe):
        raise NotImplementedError

    def get_recipe(self, identifier):
        raise NotImplementedError

    def get_recipes(self):
        raise NotImplementedError

    def get_filtered_recipes(self, filters):
        raise NotImplementedError
