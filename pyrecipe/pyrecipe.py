import logging
from random import randint

from pyrecipe.errors import RecipeError

logger = logging.getLogger()


class RecipeInator:
    def __init__(self, dbadapter):
        self.dbadapter = dbadapter

    def add_recipe(self, recipe):
        try:
            self.dbadapter.add_recipe(recipe)
        except RecipeError as e:
            logger.exception(f'Something went bad while adding a recipe: {e}')

    def get_recipe(self, identifier):
        return self.dbadapter.get_recipe(identifier)

    def get_random_recipe(self):
        # TODO: add filters to get random recipe between filtered recipes
        recipes = None
        try:
            recipes = self.dbadapter.get_recipes()
        except Exception as e:
            logger.exception(f'Error while retrieving recipes {e}')
        if recipes:
            index = randint(0, len(recipes))
            return recipes[index]

    def get_filtered_recipes(self, filters):
        # TODO: future feature
        return self.dbadapter.get_filtered_recipes(filters)
