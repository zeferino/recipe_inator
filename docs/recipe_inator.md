# Recipe Inator
## Description
This is a recipe inator. It will create a menu based on a catalog of recipes. 
From the created menu, it will create a shopping list based on the ingredients needed for 
the recipes in the menu. The menu and shopping list will be saved.

__Goal:__ Create a menu and shopping list based on a catalog of recipes.

#### Ideas
- Create a menu based on user preferences like diet, allergies.
- Create a shopping list based on the ingredients in the recipes and the menu.
- Create menu based on cooking utensils available. Ex. Microwave, oven, stove, etc.
- Create menu based on time available to cook.
- Create menu based on budget.
- Create menu based on ingredients available. (This info could be tricky to get)
- Create menu based on ingredients that are about to expire. (This info could be tricky to get)
- Create menu based on ingredients that are on sale. (This info could be tricky to get)
- Scrap info from recipe websites to create a catalog of recipes.
- Scrap product info from grocery stores to create a catalog of ingredients, with prices. (This could be a 
way of monetizing things if the app gets popular)
- Make the shopping list and try to automate the shopping process. (This could be a way of monetizing things 
if the app gets popular)
- Create a mobile app to make the shopping process easier. (This could be a way of monetizing things if the
app gets popular)



## What exists in the market

- [Eat this much](https://www.eatthismuch.com/)
- [Mealime](https://www.mealime.com/)
- [Yummly](https://www.yummly.com/)
- [Paprika](https://www.paprikaapp.com/)
- [Pepperplate](https://www.pepperplate.com/)
- [BigOven](https://www.bigoven.com/)
- [MealBoard](http://www.mealboard.com/)
- [Plan to Eat](https://www.plantoeat.com/)
- [Cook Smarts](https://www.cooksmarts.com/)
- [Meal Planner Pro](https://www.mealplannerpro.com/)
- [Meal Planner](https://www.mealplanner.com/)
- [Plan to Eat](https://www.plantoeat.com/)
- [Strong Fastr](https://www.strongrfastr.com/diet-meal-planner)

## Author
[Moises Perez](
https://www.linkedin.com/in/moisesperezespinosa/)
